# Copyright 2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_7 python3_8 )
DISTUTILS_USE_SETUPTOOLS=rdepend

inherit distutils-r1 eutils linux-info

DESCRIPTION="Services to orchestrate shared file system storage for use with virtual machines, containers and bare metals"
HOMEPAGE="https://launchpad.net/manila"

if [[ ${PV} == *9999 ]];then
    inherit git-r3
    EGIT_REPO_URI="https://github.com/openstack/manila.git"
    EGIT_BRANCH="stable/victoria"
else
    SRC_URI="https://tarballs.openstack.org/${PN}/${P}.tar.gz"
    KEYWORDS="~amd64 ~arm64 ~x86"
fi

LICENSE="Apache-2.0"
SLOT="0"
IUSE="lvm +share share-only mysql postgres sqlite zfs"
REQUIRED_USE="
    !share-only? ( || ( mysql postgres sqlite ) )
    share-only? ( share !mysql !postgres !sqlite )"

CDEPEND="
    >=dev-python/pbr-2.0.0[${PYTHON_USEDEP}]
    !~dev-python/pbr-2.1.0[${PYTHON_USEDEP}]"
DEPEND="${CDEPEND}"
RDEPEND="
    ${CDEPEND}
    >=dev-python/alembic-0.8.10[${PYTHON_USEDEP}]
    >=dev-python/eventlet-0.22.0[${PYTHON_USEDEP}]
    !~dev-python/eventlet-0.23.0[${PYTHON_USEDEP}]
    !~dev-python/eventlet-0.25.0[${PYTHON_USEDEP}]
    >=dev-python/greenlet-0.4.15[${PYTHON_USEDEP}]
    >=dev-python/lxml-4.5.0[${PYTHON_USEDEP}]
    >=dev-python/netaddr-0.7.18[${PYTHON_USEDEP}]
    >=dev-python/oslo-config-5.2.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-context-2.19.2[${PYTHON_USEDEP}]
    >=dev-python/oslo-db-5.1.1[${PYTHON_USEDEP}]
    >=dev-python/oslo-i18n-3.15.3[${PYTHON_USEDEP}]
    >=dev-python/oslo-log-3.36.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-messaging-6.4.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-middleware-3.31.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-policy-1.30.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-reports-1.18.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-rootwrap-5.8.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-serialization-2.18.0[${PYTHON_USEDEP}]
    !~dev-python/oslo-serialization-2.19.1[${PYTHON_USEDEP}]
    >=dev-python/oslo-service-2.1.1[${PYTHON_USEDEP}]
    >=dev-python/oslo-upgradecheck-0.1.0[${PYTHON_USEDEP}]
    >=dev-python/oslo-utils-3.40.2[${PYTHON_USEDEP}]
    >=dev-python/oslo-concurrency-3.26.0[${PYTHON_USEDEP}]
    >=dev-python/paramiko-2.7.1[${PYTHON_USEDEP}]
    >=dev-python/paste-2.0.2[${PYTHON_USEDEP}]
    >=dev-python/pastedeploy-1.5.0[${PYTHON_USEDEP}]
    >=dev-python/pyparsing-2.1.0[${PYTHON_USEDEP}]
    >=dev-python/python-neutronclient-6.7.0[${PYTHON_USEDEP}]
    >=dev-python/keystoneauth-3.4.0[${PYTHON_USEDEP}]
    >=dev-python/keystonemiddleware-4.17.0[${PYTHON_USEDEP}]
    >=dev-python/requests-2.14.2[${PYTHON_USEDEP}]
    >=dev-python/retrying-1.2.3[${PYTHON_USEDEP}]
    !~dev-python/retrying-1.3.0[${PYTHON_USEDEP}]
    >=dev-python/routes-2.3.1[${PYTHON_USEDEP}]
    >=dev-python/six-1.10.0[${PYTHON_USEDEP}]
    sqlite? (
        >=dev-python/sqlalchemy-1.0.10[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.5[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.6[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.7[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.8[sqlite,${PYTHON_USEDEP}]
    )
    mysql? (
        >=dev-python/pymysql-0.7.6[${PYTHON_USEDEP}]
        !~dev-python/pymysql-0.7.7[${PYTHON_USEDEP}]
        >=dev-python/sqlalchemy-1.0.10[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.5[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.6[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.7[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.8[sqlite,${PYTHON_USEDEP}]
    )
    postgres? (
        >=dev-python/psycopg-2.5.0[${PYTHON_USEDEP}]
        >=dev-python/sqlalchemy-1.0.10[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.5[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.6[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.7[sqlite,${PYTHON_USEDEP}]
        !~dev-python/sqlalchemy-1.1.8[sqlite,${PYTHON_USEDEP}]
    )
    >=dev-python/stevedore-1.20.0[${PYTHON_USEDEP}]
    >=dev-python/tooz-1.58.0[${PYTHON_USEDEP}]
    >=dev-python/python-cinderclient-3.3.0[${PYTHON_USEDEP}]
    !~dev-python/python-cinderclient-4.0.0[${PYTHON_USEDEP}]
    >=dev-python/python-novaclient-9.1.0[${PYTHON_USEDEP}]
    >=dev-python/python-glanceclient-2.15.0[${PYTHON_USEDEP}]
    >=dev-python/webob-1.7.1[${PYTHON_USEDEP}]
    lvm? ( sys-fs/lvm2 )
    zfs? ( sys-fs/zfs )
    acct-user/manila
    acct-group/manila
"

pkg_pretend() {
    linux-info_pkg_setup
    CONFIG_CHECK=""
    if use lvm; then
        CONFIG_CHECK+="~BLK_DEV_DM"
    fi
    if linux_config_exists; then
        for module in ${CONFIG_CHECK}; do
            linux_chkconfig_present ${module} || ewarn "${module} needs to be enabled"
        done
    fi
}

python_install_all() {
    distutils-r1_python_install_all

    #for svc in ; do
        #newinitd "${FILESDIR}/manila.initd" manila-${svc}
    #done

    diropts -m 0750 -o manila -g manila
    dodir /var/log/manila /var/lib/manila
    keepdir /etc/manila
    keepdir /var/log/manila
    keepdir /var/lib/manila

    insinto /etc/manila
    insopts -m 0640 -o manila -g manila
    doins -r etc/manila/*.ini etc/manila/*.conf etc/manila/rootwrap.d
    newins "${FILESDIR}/manila.conf.sample" "manila.conf.sample"

    distutils-r1_python_install_all
    rm -r "${ED}"/usr/etc
}


















