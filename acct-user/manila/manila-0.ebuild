# Copyright 2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit acct-user

DESCRIPTION="user for the manila openstack service"
ACCT_USER_ID=450
ACCT_USER_HOME=/var/lib/manila
ACCT_USER_HOME_PERMS=0770
ACCT_USER_GROUPS=( manila )

acct-user_add_deps
